# GIT GLOBAL SETUP
```bash
git config --global user.name "<your_name_at_GitLab>"
git config --global user.email "<your_email_at_GitLab>"
# You can also set this on a per-repository basis using the option "--local"
# from within the root directory of your local repo
```
Some general settings that make life easier
```bash
# Set global default text editor that Git will use whenever you have to type
# in a message (I don't really like vim, which is the default editor)
git config --global core.editor <editor_you_like> # e.g. 'emacs'
git config --list # sse your global git settings
``` 

# CLONING A REPO FROM GitLab (SSH):
```bash
cd /path/to/dir/where/you/want/your/repo
git clone <SSH-url-to-repo-in-GitLab> # See list of repos below
cd <dir-you-just-cloned>
# When you want to commit your changes:
git commit -m "my_message"
git push -u origin master
# IMPORTANT: always include option '-u' to set upstream branch being followed by your local repo
```

# OTHER OPTIONS FOR CREATING REPOS AT GitLab
## Pushing existing folder as origin branch to GitLab
```bash
cd existing_folder
git init
git remote add origin <SSH-url-to-repo-in-GitLab>
git add -A
git commit -m <"my_message">
git push -u origin master
# IMPORTANT: always include option '-u' to set upstream branch being followed by your local repo
```
## Adding a remote GitLab repo to an existing LOCAL Git repository
```bash
cd existing_local_repo
git remote add origin <SSH-url-to-repo-in-GitLab>
git commit -m <"my_message">
git push -u origin --all
git push -u origin --tags
# IMPORTANT: always include option '-u' to set upstream branch
```

# SIGNING YOUR COMMITS WITH GnuPG

#### Since we're not signing our commits, you can safely ignore this

```bash
# Check that your key is present under gpg:
gpg --list-keys
# If your GnuPG key was generated with gpg2, you must first export it
# from gpg2 (also possible through a GUI. like  seahorse):
gpg2 --export <key_ID_Nr> --output </path/to/output/>
# And then import the key it to gpg:
gpg --import <path/to/exported/key>
# take a look at the keys managed by gpg (notice the 'pub' label):
gpg --list-keys
# Now tell Git which key in the gpg keyring to use for signing your commits
git config --global user.signingkey <your_private_(pub)_GPG_key_ID>
# If you want to GPG sign all your commits, you have to always add the -S
# option.
# If you wish to automatically sign all your commits in a particular repo, 
# toggle the 'commit.gpgsign' configuration option in that repo:
cd /path/to/repo/needing/gpg/signature
git config commit.gpgsign true
# After a commit, check if your signature is working:
cd /path/to/your/repo
git log --show-signature -1
```
#  Git force pull to overwrite local files
```bash
git fetch --all
git reset --hard origin/master
git pull origin master
```
