# Instructions for getting Julia (0.5.1) to work

;-)

_These instructions assumea a functioning Julia and Emacs (25+) installation._

## Required Fedora 25 software packages

_These packages might have another name if your system is DEB based_

```bash
emacs
screen
```

## Required Emacs packages

```emacs
julia-mode
julia-shell
ess
```

### Suggested Emacs packages
electric-operator

#### Suggested Emacs minor modes

Activating following minor modes makes coding far easier in Emacs:

```emacs
electric-operator
electric-indent-mode
electric-pair-mode
auto-complete-mode
julia-shell
```
`julia-shell` creates a Julia REPL in an Emacs `shell` buffer.

## Installing basic Julia packages

*The prefix "julia>" denotes commands entered in the Julia REPL*

```julia
julia>Pkg.add("DataFrames")
```
https://dataframesjl.readthedocs.io/en/latest/introduction.html

## Reading CSV tables in

```julia
julia> using DataFrames
julia> dataframe = readtable(path/to/file.csv)
```
