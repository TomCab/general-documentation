# Working with branches

## Branch naming conventions
1. Use grouping tokens (words) at the beginning of your branch names.
2. Define and use short lead tokens to differentiate branches in a way that is meaningful to your workflow.
3. Use slashes to separate parts of your branch names.
4. Do not use bare numbers as leading parts.
5. Avoid long descriptive names for long-lived branches.

## HowTo manage our branches
### Whenever you start working on a new feature of the project, create a new branch
```bash
cd /path/to/local/repo
git checkout -b <my/new/branch>
```
This creates a `HEADER` pointing to this branch.
From now on you're working on this branch until either
* you checkout another branch, or
* you merge this branch to the `master` branch

After your changes, you can `commit`, `push` and `fetch` normally to this branch, without affecting the `master` branch:

```bash
git commit -A -m "<my-message>"
git push -u origin  <my/new/branch>
git fetch origin
```
### Viewing and switching your branches
You can view the branches linked to your local repository by:
```bash
git branch -vv
```
And you can switch between branches by:
```bash
git checkout <branch/name>
```
### Merging a branch with `master`
When you are finished developing the new feature or aspect and everything looks OK, merge `<my/new/branch>` with the `master` branch:

```bash
cd /path/to/<my/new/branch>
git add -A # stage all files (including deletions)
git commit -m "<my_commit_message>"
git push -u origin  <my/new/branch>
# This is just to be sure that everything is up to date
git checkout master # change your HEAD to branch 'master'
git merge <my/new/branch> # merge <my/new/branch>  into 'master' branch
git branch -d <my/new/branch> # delete the <my/new/branch> branch
```

# More info on working with branches:

https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging

https://stackoverflow.com/questions/273695/git-branch-naming-best-practices#6065944
