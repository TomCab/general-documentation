# Installing Julia Programming Language (stable release)

_These instructions are tailored to a Fedora 25 system. Please adapt to your own setup._


Enable the Julia repository:
```bash
sudo dnf copr enable nalimilan/julia
```
Install Julia:
```bash
sudo dnf install julia
```

## More information:

https://julialang.org/downloads/platform.html

