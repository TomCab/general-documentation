# GENERATE SSH KEYS AND ADD THEM TO SSH AGENT

 List existing SSH keys, so as not to overwrite them
```bash
ls -al ~/.ssh/
```
Generate a new key, using the e-mail associated with your account at the server you want to connect to:
```bash
ssh-keygen -t rsa -b 4096 -C "<my_email>"
```
Start the SSH agent in the background
```bash
eval "$(ssh-agent -s)"
```
Add the generated key to the agent
```bash
ssh-add ~/.ssh/<name_of_generated_key> 
```

# ADD THE GENERATED KEY TO THE SERVER
This step is server-specific. But in general: you copy to the server the PUBLIC key, e.g.:
```bash
~/.ssh/<name_of_generated_key>.pub
```

# CHECK INSTALLATION AND CONNECTIVITY
Check whether the added key is included in current client SSH identities:
```bash
ssh-add -l
```
Check that you can connect to your server through SSH:
```bash
ssh -vT git@<my_server>
```
You should see at the end of a lot of debug  messages something like:
```bash
...
Welcome to <server-name> <your-user-name>
...
debug1: Exist status 0
```
