# Installing and running Beaker Notebook

_These instructions are tailored to a Fedora 25 system. Adapt to your particular setup._


## Install Docker CE.

Check whether `dnf-plugins-core` package is installed.

Install the Docker repository:
```bash
sudo dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
```
Update the `dnf` package index:
```bash
sudo dnf makecache fast
```
Install the latest version of Docker Community Edition:
```bash
sudo dnf install docker-ce
```
Check that the signature matches and accept:
```bash
060A 61C5 1B55 8A7F 742B 77AA C52F EB6B 621E 9F35
```
Start Docker:
```bash
sudo systemctl start docker
```
Verify that Docker CE is installed correctly by running the hello-world image:
```bash
sudo docker run hello-world
```
This command downloads a test image and runs it in a container. When the container runs, it prints an informational message and exits.
 You need sudo in order to run Docker commands.
 Configure Docker to start on boot:
```bash
sudo systemctl enable docker
```
To disable this behavior, use `disable` instead.
Update system services:
```bash
sudo chkconfig docker on
```
You can start/stop/restart the docker daemon using:
```bash
sudo systemctl start docker
sudo systemctl stop docker
sudo systemctl restart docker
```

**NOTICE: there are some security issues associated with running a Docker container with administrative role. Please see:**

https://docs.docker.com/engine/security/security/

### More information on Docker CE:

https://docs.docker.com/engine/installation/linux/fedora/#install-using-the-repository

https://docs.docker.com/engine/installation/linux/linux-postinstall/

https://docs.docker.com/engine/userguide/


## Install and run Beaker Notebook
With docker running, execute from your home directory (at first installation, docker downloads a 4.7 GiB image) the following one-liner script:
```bash
sudo docker run -v $HOME:/home/beaker/host -p 127.0.0.1:8801:8801 -t beakernotebook/beaker su -m beaker -c 'export PATH=$PATH:/usr/sbin && /home/beaker/src/core/beaker.command --connect-host 127.0.0.1 --listen-interface \*'
```
Enter following address in your browser: `http://127.0.0.1:8801/`


**NOTICE: there are some security issues associated with running a Docker container with administrative role. Please see:**

https://docs.docker.com/engine/security/security/

### More information on Beaker Notebook:

https://github.com/twosigma/beakerx/wiki/Running-the-Docker-Container

http://beakernotebook.com/index
